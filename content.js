const ATTEMPTS_LIMIT = 4
const CONFLUENCE_HEADER_TABLE = 'tablesorter-headerRow'

const STATUS_GROUPING = {
  POOL: 'POOL',
  'RESEARCH & ANALYSIS': 'UX',
  PROTOTYPING: 'UX',
  'UX REVIEW': 'UX',
  'READY FOR DEV': 'UX',
  'TO DO': 'TO_DO',
  'IN PROGRESS': 'IN_PROGRESS',
  'IN REVIEW': 'IN_PROGRESS',
  'DEV COMPLETE': 'DONE',
  'READY FOR TESTING': 'DONE',
  'QA TESTING': 'DONE',
  'STAGE TESTING': 'DONE',
  'DEPLOY READY': 'DONE',
  CLOSED: 'DONE',
  BLOCKED: 'BLOCKED',
}

const GROUP_COLOR = {
  POOL: '#F4F5F7',
  UX: '#FFF0FA',
  TO_DO: '#FFF',
  IN_PROGRESS: '#E6F9FE',
  QA: '#FEFEDC',
  DONE: '#d5f1d7',
  BLOCKED: '#D9D9D9',
}

function updateStatus(attemptNr) {
  if (attemptNr > ATTEMPTS_LIMIT) {
    return
  }

  const confluenceTable = document.getElementsByClassName('confluenceTable')

  console.log(2222, confluenceTable)
  if (!confluenceTable.length || !confluenceTable[0]) {
    return
  }

  let tableRows = confluenceTable[0].rows
  let jiraStatus = null
  let jiraCell = null
  let row = null
  let statusGroup = null

  for (let i = 0, n = tableRows.length; i < n; i++) {
    row = tableRows[i]

    if (row.className !== CONFLUENCE_HEADER_TABLE) {
      jiraCell = row.getElementsByClassName(
        'jira-macro-single-issue-export-pdf'
      )

      // In case the status was not loaded yet, we retry 1 second later.
      // if (!jiraCell.length) {
      //   callStatusUpdate(attemptNr + 1)
      //   return
      // }

      jiraStatus = jiraCell[0] ? jiraCell[0].innerText : null
      statusGroup = STATUS_GROUPING[jiraStatus]

      if (statusGroup) {
        row.bgColor = GROUP_COLOR[statusGroup]
      }
    }
  }
}

function callStatusUpdate(attemptNr) {
  window.setTimeout(() => updateStatus(attemptNr), 3000)
}

callStatusUpdate(0)
